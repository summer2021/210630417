# 小结

作为一名应届毕业生，迫于就业压力，我不得不花费大量时间在求职活动上。本项目的时间区间为七到九月，而今年在七月初就开始秋招的企业更多了，导致项目前期的时间远不及我预想的充裕。这是我决定中止此项目的首要原因。

已提交的代码还没实现一个核心功能，那就是 ready list 的维护。还有一些重要的功能，如 epoll 对象的嵌套，即通过一个 epoll 对象监视另一个 epoll 对象，涉及到循环检测，比较复杂。

## 给后继者的帮助

1. Linux epoll 的实现在 [`fs/eventpoll.c`](https://github.com/torvalds/linux/blob/master/fs/eventpoll.c) 和 [`include/uapi/linux/eventpoll.h`](https://github.com/torvalds/linux/blob/master/include/uapi/linux/eventpoll.h) 中，里面有一些会让初次阅读内核代码的人迷惑的宏，但搜索可以轻松找到答案。

2. zCore 目前缺乏详细的文档，要知道有哪些已实现的设施可以利用只能多读代码。下面是我陆续找到的一些需要用到的实现，以免后人重复做功：

    `linux-object/src/fs/mod.rs`
    - `struct FileDesc`: 是一个 _tuple struct_, 内含一个`i32`, 是对整数文件描述符的表示。
    - `trait FileLike`: 类文件对象要实现的接口。在 Linux 中则是通过检查 `f_op` 来获知一个文件是否支持某个操作。

    `linux-object/src/fs/file.rs`
    - `struct File`: 相当于 Linux 的 _open file description_

    `linux-object/src/process.rs`
    - `struct LinuxProcess`: Linux 进程的表示，系统调用可以通过`self.linux_process()`拿到发起调用的进程。下面是文件操作要用到的一些方法：
        - `fn add_file`: Add a file to the file descriptor table.
        - `fn add_file_at`: Add a file to the file descriptor table at given `fd`.
        - `fn insert_file`: insert a file and fd into the file descriptor table
        - `fn get_file`: Get the `File` with given `fd`.
        - `fn get_file_like`: Get the `FileLike` with given `fd`.
        - `fn close_file`: Close file descriptor `fd`.

3. 编写异步系统调用可以参考已经实现的 `poll` 和 `select` ，在 [`poll.rs`](https://github.com/rcore-os/zCore/blob/master/linux-syscall/src/file/poll.rs) 中。
